variable "prefix" {
  default = "raad"

}

variable "project" {
  default = "recipe-app-api-devops"

}

variable "contact" {

  default = "jokaguitar@gmail.com"

}

variable "db_username" {
  description = "user name for the RDS postgress instance"

}

variable "db_password" {
  description = "Password for the RDS posgress instance"

}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"

}